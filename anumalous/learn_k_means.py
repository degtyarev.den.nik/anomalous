#!/usr/bin/env python

from __future__ import print_function
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

import data_reader
import learn_utils_k_means

WINDOW_LEN = 32


def get_windowed_segments(data, window):
    step = 2
    windowed_segments = []
    segments = learn_utils_k_means.sliding_chunker(
        data,
        len(window),
        step
    )
    for segment in segments:
        segment *= window
        windowed_segments.append(segment)
    return windowed_segments


def main():
    n_samples = 8000
    print("Reading data...")
    data = data_reader.read_data()[0]

    # Some values goes zero to looks like anomalous
    data_anomalous = np.copy(data)
    data_anomalous[200:220] = 0

    window_rads = np.linspace(0, np.pi, WINDOW_LEN)
    window = np.sin(window_rads) ** 2
    print("Windowing data...")
    windowed_segments = get_windowed_segments(data, window)

    print("Clustering...")
    clusterer = KMeans(150)
    clusterer.fit(windowed_segments)

    print("Reconstructing...")
    # reconstruction = learn_utils.reconstruct(data, window, clusterer)
    reconstruction = learn_utils_k_means.reconstruct(data_anomalous, window, clusterer)
    # error = reconstruction - data
    error = reconstruction[0:n_samples] - data_anomalous[0:n_samples]
    error_99th_percentile = np.percentile(error, 99)
    print("Maximum reconstruction error was %.1f" % error.max())
    print("99th percentile of reconstruction error was %.1f" % error_99th_percentile)

    plt.figure()
    n_plot_samples = 300
    plt.plot(data[0:n_plot_samples], label="Original")
    plt.plot(reconstruction[0:n_plot_samples], label="Reconstructed")
    plt.plot(error[0:n_plot_samples], label="Reconstruction error")
    plt.legend()
    plt.show()