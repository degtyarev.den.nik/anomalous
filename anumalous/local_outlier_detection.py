import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import LocalOutlierFactor
from numpy.random import seed
from sklearn.preprocessing import MinMaxScaler

import data_reader
from not_used import ekg_data
import pandas as pd


def lof():
    n_samples = 100
    # data = ekg_data.read_ekg_data('not_used/a02.dat')[0:n_samples]
    # time_array = np.arange(n_samples)
    # df = pd.DataFrame({"value": data, "time": time_array})
    # df = pd.get_dummies(df)
    data = data_reader.make_data()

    scaler = MinMaxScaler()
    num = data.iloc[:,1:]
    # num2 = scaler.inverse_transform(num)
    # num2 = pd.DataFrame(num2, columns=num2.columns)

    clf = LocalOutlierFactor(n_neighbors=20)
    y_pred = clf.fit_predict(data)
    scores = clf.negative_outlier_factor_
    pred = pd.Series(y_pred).replace([-1, 1], [1, 0])
    anomalies = data[pred == 1]

    cmap = np.array(["white", "red"])
    plt.scatter(data.iloc[:, 0], data.iloc[:, 1], c="white", s = 20, edgecolor ="k")
    plt.scatter(anomalies.iloc[:, 0], anomalies.iloc[:, 1], c="red")
    # ,marker=’x’,s=100)
    plt.title("Local Outlier Factor — Anomalies")
    plt.xlabel("Income")
    plt.ylabel("Spend_Score")
    plt.show()

# only for test with test data
# def test_local_outlier_detection():
#     np.random.seed(42)
#
#     # Generate train data
#     X = 0.3 * np.random.randn(100, 2)
#     # Generate some abnormal novel observations
#     X_outliers = np.random.uniform(low=-4, high=4, size=(20, 2))
#     X = np.r_[X + 2, X - 2, X_outliers]
#
#     # fit the model
#     clf = LocalOutlierFactor(n_neighbors=20)
#     y_pred = clf.fit_predict(X)
#     y_pred_outliers = y_pred[200:]
#
#     # plot the level sets of the decision function
#     xx, yy = np.meshgrid(np.linspace(-5, 5, 50), np.linspace(-5, 5, 50))
#     Z = clf._decision_function(np.c_[xx.ravel(), yy.ravel()])
#     Z = Z.reshape(xx.shape)
#
#     plt.title("Local Outlier Factor (LOF)")
#
#     a = plt.scatter(X[:200, 0], X[:200, 1], c='white',
#                     edgecolor='k', s=20)
#     b = plt.scatter(X[200:, 0], X[200:, 1], c='red',
#                     edgecolor='k', s=20)
#     plt.axis('tight')
#     plt.xlim((-5, 5))
#     plt.ylim((-5, 5))
#     plt.legend([a, b],
#                ["normal observations",
#                 "abnormal observations"],
#                loc="upper left")
#     plt.show()