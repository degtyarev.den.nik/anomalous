import pandas as pd
from adtk.data import validate_series
from adtk.transformer import RegressionResidual, DoubleRollingAggregate
from adtk.visualization import plot
from sklearn.linear_model import LinearRegression
from adtk.transformer import PcaReconstructionError
from adtk.transformer import ClassicSeasonalDecomposition

import data_reader


def test():
    data = data_reader.make_data()
    data = validate_series(data)
    change_value(data)


def regression_residual(data):
    transformed_data = RegressionResidual(regressor=LinearRegression(), target="data")\
        .fit_transform(data)\
        .rename("Regression residual")
    plot(pd.concat([data, transformed_data], axis=1), ts_linewidth=1,
         ts_markersize=3,
         curve_group=[("data"), "output"])


def pca_reconstruction_errors(data):
    s = PcaReconstructionError(k=1).fit_transform(data).rename("PCA Reconstruction Error")
    plot(pd.concat([data, s], axis=1), ts_linewidth=1, ts_markersize=3,
         curve_group=[("data"), "PCA Reconstruction Error"]);


def seasonal_decomposition_residual(data):
    data_transformed = ClassicSeasonalDecomposition().fit_transform(data).rename("Seasonal decomposition residual")
    plot(pd.concat([data, data_transformed], axis=1), ts_markersize=1)


def seasonal_decomposition_trends(data):
    data_transformed = ClassicSeasonalDecomposition(freq=7, trend=True).fit_transform(data).rename(
        "Seasonal decomposition residual")
    plot(pd.concat([data, data_transformed], axis=1), ts_linewidth=1, ts_markersize=4);


def change_value(data):
    data_transformed = DoubleRollingAggregate(
        agg="quantile",
        agg_params={"q": [0.1, 0.5, 0.9]},
        window=50,
        diff="l2").transform(data).rename("Diff rolling quantiles")
    plot(pd.concat([data, data_transformed], axis=1))


def shift_value(data):
    data_transformed = DoubleRollingAggregate(
        agg="median",
        window=5,
        diff="diff").transform(data).rename("Diff rolling median")
    plot(pd.concat([data, data_transformed], axis=1));


def mean_window(data):
    data_transformed = DoubleRollingAggregate(
        agg="mean",
        window=(3, 1),  # The tuple specifies the left window to be 3, and right window to be 1
        diff="l1").transform(data).rename("Diff rolling mean with different window size")
    plot(pd.concat([data, data_transformed], axis=1), ts_linewidth=1, ts_markersize=3);