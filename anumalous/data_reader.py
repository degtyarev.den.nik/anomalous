import h5py
import pandas as pd

filename = "data.h5"
samples = 8000


def read_data():
    with h5py.File(filename, "r") as f:
        # List all groups
        print("Keys: %s" % f.keys())
        # a_group_key = list(f.keys())[0]

        # Get the data
        sensor_data = f["StrSensorMk1"][0:8000]
        time_data = f["Time"][0:8000]
        f.close()
        return sensor_data, time_data


def make_data():
    data = read_data()
    print("reading....")
    sensor_data = data[0]
    time_data = pd.Series(pd.to_datetime(data[1], unit='s'))
    df = pd.DataFrame(sensor_data, index=time_data, columns=["value"])
    return df
