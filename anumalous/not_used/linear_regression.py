from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error

import data_reader


def regression():
    #todo do data
    X_train, X_test, y_train, y_test = data_reader.make_data().get_dataset(dataset.Users, test_size=0.3, lag_start=12, lag_end=48)
    lr = LinearRegression()
    lr.fit(X_train, y_train)
    prediction = lr.predict(X_test)
    plt.figure(figsize=(15, 7))
    plt.plot(prediction, "r", label="prediction")
    plt.plot(y_test.values, label="actual")
    plt.legend(loc="best")
    plt.title("Linear regression\n Mean absolute error {} users".format(round(mean_absolute_error(prediction, y_test))))
    plt.show()