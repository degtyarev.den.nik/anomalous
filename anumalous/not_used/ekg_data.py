#!/usr/bin/env python

import numpy as np
import struct
import matplotlib.pyplot as plt
import pandas as pd

def get_dataset(number):
    data = read_ekg_data("a02.dat")[0:number]
    time_array = np.arange(number)
    df = pd.DataFrame({"value": data, "time": time_array})
    df = pd.get_dummies(df)
    return df

def read_ekg_data(input_file):
    with open(input_file, 'rb') as input_file:
        data_raw = input_file.read()
    n_bytes = len(data_raw)
    n_shorts = n_bytes/2
    # data is stored as 16-bit samples, little-endian
    # '<': little-endian
    # 'h': short
    unpack_string = '<%dh' % n_shorts
    # sklearn seems to throw up if data not in float format
    data_shorts = np.array(struct.unpack(unpack_string, data_raw)).astype(float)
    return data_shorts

def plot_ekg(input_file, n_samples):
    ekg_data = read_ekg_data(input_file)
    plt.plot(ekg_data[0:n_samples])
    plt.show()
