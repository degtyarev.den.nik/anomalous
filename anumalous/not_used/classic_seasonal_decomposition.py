from adtk.transformer import ClassicSeasonalDecomposition
from adtk.visualization import plot
from statsmodels.compat import pandas as pd


def seasonal_decomposition_residual(data):
    data_transformed = ClassicSeasonalDecomposition().fit_transform(data).rename("Seasonal decomposition residual")
    plot(pd.concat([data, data_transformed], axis=1), ts_markersize=1)


def seasonal_decomposition_trends(data):
    data_transformed = ClassicSeasonalDecomposition(freq=7, trend=True).fit_transform(data).rename(
        "Seasonal decomposition residual")
    plot(pd.concat([data, data_transformed], axis=1), ts_linewidth=1, ts_markersize=4);